(defproject neanderthal-bench "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [criterium "0.4.4"]
                 [uncomplicate/neanderthal "0.20.3"]
                 [uncomplicate/fluokitten "0.6.1"]
                 [uncomplicate/commons "0.5.0"]
                 [org.slf4j/slf4j-nop "1.7.25"]]
  :main neanderthal-bench.core
  :repl-options {:init-ns neanderthal-bench.core})
