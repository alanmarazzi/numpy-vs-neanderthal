#!/usr/bin/env bash

python numpy_bench.py \
  --fast \
  -o "bench.json" && \
  python -m perf \
         stats bench.json
